package googlehashcode17;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.hamcrest.core.Is;
import org.junit.Test;

public class AlgoTest {
	@Test
	public void prioCache() throws Exception {
		// TODO prio caches
		// sum latencys
		// sum connectedEndpoints

		Algo cut = new Algo();
		List<Cache> caches = new LinkedList();
		caches.add(CacheTest.creatShittyCache(0));
		List<Integer> priodCacheIds = cut.prioCaches(caches);
		assertThat(priodCacheIds.get(0), Is.is(0));
	}

	@Test
	public void prioCaches() throws Exception {
		// TODO prio caches
		// sum latencys
		// sum connectedEndpoints

		Algo cut = new Algo();
		List<Cache> caches = new LinkedList();
		caches.add(CacheTest.createShittyCache(0, 2));
		caches.add(CacheTest.createShittyCache(2, 1));
		List<Integer> priodCacheIds = cut.prioCaches(caches);
		assertThat(priodCacheIds.get(0), Is.is(2));
	}

	@Test
	public void testName() throws Exception {
		Algo cut = new Algo();

		List<Request> requests = new LinkedList();
		requests.add(new Request(0, 1, 0));
		requests.add(new Request(0, 2, 0));
		requests.add(new Request(0, 3, 0));
		List<Video> videos = new LinkedList<>();
		videos.add(new Video(110, 1));
		videos.add(new Video(110, 3));
		Videos v = new Videos();
		v.requests = requests;
		v.videos = videos;
		int cacheSize = 100;
		List<Request> relevantRequests = cut.filter(v, cacheSize);
		assertThat(relevantRequests.get(0).videoId, Is.is(2));
		// videos aussortieren aus den requestliste

		// filter requests by endpoint ids
		// prio requests by viewcount ->videoIds
		// fill cache
		// delete all requests for videos in cache

	}
	
	@Test
	public void filterByEndpointId() throws Exception {
		List<Request> requests = new LinkedList();
		requests.add(new Request(0, 1, 0));
		requests.add(new Request(0, 2, 0));
		requests.add(new Request(0, 3, 0));
		Algo cut = new Algo();
		List<Request> request =cut.filterByEndpointId(requests,CacheTest.createShittyCache(0, 1));
		assertThat(request.size(), Is.is(3));
		
	}
	@Test
	public void prio() throws Exception {
		List<Request> requests = new LinkedList();
		requests.add(new Request(1, 1, 0));
		requests.add(new Request(10, 2, 0));
		requests.add(new Request(100, 3, 0));
		List<Video> videos = new LinkedList<>();
		videos.add(new Video(110, 1));
		videos.add(new Video(110, 2));
		videos.add(new Video(110, 3));
		Videos v = new Videos();
		v.requests = requests;
		v.videos = videos;
		Algo cut = new Algo();
		List<Integer> prioVideoIds =cut.prio(requests,v);
		assertThat(prioVideoIds.get(0), Is.is(3));
	}
	
}
