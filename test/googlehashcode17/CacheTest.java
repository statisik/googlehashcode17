package googlehashcode17;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.hamcrest.core.Is;
import org.junit.Test;

public class CacheTest {

	@Test
	public void sumCaches1() throws Exception {
		// sum latencys
		// sum connectedEndpoints
		int cacheId = 1;
		Cache cut = new Cache();

		cut.endpoints = new LinkedList<Endpoint>();
		cut.endpoints.add(new Endpoint());
		cut.endpoints.get(0).caches = new LinkedList<>();
		List<CacheData> caches = cut.endpoints.get(0).caches;
		CacheData cd = new CacheData();
		cd.id = cacheId;
		cd.latency = 300;
		caches.add(cd);

		int sum = cut.sumLatency();
		assertThat(sum, Is.is(300));

	}

	@Test
	public void sumCaches2() throws Exception {
		// sum latencys
		// sum connectedEndpoints
		int cacheId = 1;
		Cache cut = new Cache();

		cut.endpoints = new LinkedList<Endpoint>();
		cut.endpoints.add(new Endpoint());
		cut.endpoints.get(0).caches = new LinkedList<>();
		List<CacheData> caches = cut.endpoints.get(0).caches;
		CacheData cd = new CacheData();
		cd.id = cacheId;
		cd.latency = 300;
		caches.add(cd);

		CacheData cd2 = new CacheData();
		cd2.id = cacheId;
		cd2.latency = 300;
		caches.add(cd2);
		int sum = cut.sumLatency();
		assertThat(sum, Is.is(600));

	}

	@Test
	public void sumConnectedEndpoints() throws Exception {
		int cacheId = 1;
		Cache cut = creatShittyCache(cacheId);
		int sum = cut.sumConnectedEndpoints();
		assertThat(sum, Is.is(2));

	}

	public static Cache creatShittyCache(int cacheId) {
		int latency = 300;
		return createShittyCache(cacheId, latency);
	}

	public static Cache createShittyCache(int cacheId, int latency) {
		Cache cut = new Cache();
		cut.id = cacheId;

		cut.endpoints = new LinkedList<Endpoint>();
		cut.endpoints.add(new Endpoint());
		cut.endpoints.get(0).caches = new LinkedList<>();
		List<CacheData> caches = cut.endpoints.get(0).caches;
		CacheData cd = new CacheData();
		cd.id = cacheId;
		cd.latency = latency;
		caches.add(cd);

		CacheData cd2 = new CacheData();
		cd2.id = cacheId;
		cd2.latency = latency;
		caches.add(cd2);
		return cut;
	}
}
