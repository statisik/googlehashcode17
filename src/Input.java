import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import googlehashcode17.CacheData;
import googlehashcode17.Endpoint;
import googlehashcode17.Request;
import googlehashcode17.Video;

public class Input {
	public int anzVideos;
	public int anzEndpoints;
	public int anzRequestDescriptions;
	public int anzCaches;
	public int cacheSize;
	ArrayList<String> txtDateiZeilen = new ArrayList();

	@Override
	public String toString() {
		return "Input [anzVideos=" + anzVideos + ", anzEndpoints=" + anzEndpoints + ", anzRequestDescriptions="
				+ anzRequestDescriptions + ", anzCaches=" + anzCaches + ", cacheSize=" + cacheSize + "]";
	}

	public Input(String filename) throws IOException {
		FileReader fr = new FileReader(filename);
		BufferedReader br = new BufferedReader(fr);

		String temp = "";
		while (true) {
			temp = br.readLine();
			if (temp == null) {
				break;
			}
			txtDateiZeilen.add(temp);
		}
		br.close();

		System.out.println("Anzahl Zeilen: " + txtDateiZeilen.size());

		String[] firstLine = txtDateiZeilen.get(0).split(" ");
		anzVideos = Integer.parseInt(firstLine[0]);
		anzEndpoints = Integer.parseInt(firstLine[1]);
		anzRequestDescriptions = Integer.parseInt(firstLine[2]);
		anzCaches = Integer.parseInt(firstLine[3]);
		cacheSize = Integer.parseInt(firstLine[4]);

	}

	public List<Request> getRequestList() {
		List<Request> list = new ArrayList();
		for (int i = txtDateiZeilen.size() - anzRequestDescriptions; i < txtDateiZeilen.size(); i++) {
			String[] thisLine = txtDateiZeilen.get(i).split(" ");
			list.add(new Request(Integer.parseInt(thisLine[2]), Integer.parseInt(thisLine[0]),
					Integer.parseInt(thisLine[1])));
		}
		return list;
	}

	public List<Video> getVideoList() {
		List<Video> list = new ArrayList();
		String[] secondLine = txtDateiZeilen.get(1).split(" ");
		for (int i = 0; i < secondLine.length; i++) {
			list.add(new Video(Integer.parseInt(secondLine[i]), i));
		}
		return list;
	}

	public Endpoint getEndpoint(int id) {
		int i = 0;
		int lineOfEndpoint = 2;
		for (i = 0; i < id; i++) {
			lineOfEndpoint = getNextEndpoint(lineOfEndpoint);
		}
		String[] endpointLine = txtDateiZeilen.get(lineOfEndpoint).split(" ");

		return new Endpoint(getCacheData(lineOfEndpoint), Integer.parseInt(endpointLine[0]), i);
	}

	public int getNextEndpoint(int lineOfPreviousEndpoint) {
		int next = Integer.parseInt(txtDateiZeilen.get(lineOfPreviousEndpoint).split(" ")[1]);
		return next + lineOfPreviousEndpoint +1;
	}

	public List<CacheData> getCacheData(int lineOfEndpoint) {
		List<CacheData> list = new ArrayList();
		int numberOfCaches = Integer.parseInt(txtDateiZeilen.get(lineOfEndpoint).split(" ")[1]);
		for (int i = 1 + lineOfEndpoint; i <= lineOfEndpoint + numberOfCaches; i++) {
			list.add(new CacheData(Integer.parseInt(txtDateiZeilen.get(i).split(" ")[0]), // id
					Integer.parseInt(txtDateiZeilen.get(i).split(" ")[1])));// latency
		}
		return list;
	}

	public static void main(String[] args) throws IOException {
		Input i1 = new Input("test.txt");
		System.out.println(i1);
		List temp = i1.getRequestList();
		List temp2 = i1.getVideoList();
		System.out.println(temp);
		System.out.println(temp2);

		Endpoint temp3 = i1.getEndpoint(1);
		System.out.println(temp3);
	}

}
