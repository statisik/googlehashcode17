package googlehashcode17;

public class Request {
	public int count;
	public int videoId;
	public int endpointId;
	
	public Request(int count, int videoId, int endpointId) {
		this.count = count;
		this.videoId = videoId;
		this.endpointId = endpointId;
	}

	@Override
	public String toString() {
		return "Request [count=" + count + ", videoId=" + videoId + ", endpointId=" + endpointId + "]";
	}
}
