package googlehashcode17;

public class CacheData {
	public int id;
	public int latency;
	
	public CacheData(int id, int latency) {
		super();
		this.id = id;
		this.latency = latency;
	}

	public CacheData() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "CacheData [id=" + id + ", latency=" + latency + "]";
	}
	
}
