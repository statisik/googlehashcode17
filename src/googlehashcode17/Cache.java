package googlehashcode17;

import java.util.List;

public class Cache {

	public List<Endpoint> endpoints;
	public int id;

	public  int sumLatency() {
		int cacheId=id;
		int sum=0;
		for (Endpoint endpoint : endpoints) {
			List<CacheData> caches = endpoint.caches;
			for (CacheData cacheData : caches) {
				if(cacheData.id==cacheId){
					sum+=cacheData.latency;
				}
			}
		}
		return sum;
	}

	public int sumConnectedEndpoints() {
		int cacheId=id;
		int sum=0;
		for (Endpoint endpoint : endpoints) {
			List<CacheData> caches = endpoint.caches;
			for (CacheData cacheData : caches) {
				if(cacheData.id==cacheId){
					sum++;
				}
			}
		}
		return sum;	}

}
