package googlehashcode17;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Algo {

	public List<Integer> prioCaches(List<Cache> caches) {
		Map<Double,Integer>weights= new HashMap<Double, Integer>();
		for (int i = 0; i < caches.size(); i++) {
			Cache cache = caches.get(i);
			double weight = (double)cache.sumLatency()/(double)cache.sumConnectedEndpoints();
			weights.put( weight,cache.id);
		}
		 Map<Double,Integer> treeMap = new TreeMap<Double,Integer>(weights);
		 List<Integer>returnStuff=new LinkedList<Integer>();
		 for(Map.Entry<Double,Integer> entry : treeMap.entrySet()) {
			returnStuff.add(entry.getValue());
		 }
		return returnStuff;
	}

	public List<Request> filter(Videos v, int cacheSize) {
		List<Integer>badVideoIds=new LinkedList<Integer>();
		List<Video> videos = v.videos;
		for (Video video : videos) {
			if(video.size>=cacheSize){
				badVideoIds.add(video.id);
			}
		}
		List<Request> requests = v.requests;
		List<Request> godsRequests = new LinkedList<Request>();
		for (Request request : requests) {
			if(!badVideoIds.contains(request.videoId)){
				godsRequests.add(request);
			}
		}
		return godsRequests;
	}

	public List<Request> filterByEndpointId(List<Request> requests, Cache cache) {
		List<Request> returns = new LinkedList<Request>();
		for (Request request : requests) {
			List<Endpoint> endpoints = cache.endpoints;
			for (Endpoint endpoint : endpoints) {
				if(request.endpointId==endpoint.id){
					returns.add(request);
				}
			}
		}
		return returns;
	}

	public List<Integer> prio(List<Request> requests, Videos v) {
		Map<Double,Integer> prioVideo =new HashMap<>();
		for (Request request : requests) {
			prioVideo.put((double)request.count/find(v, request.videoId), request.videoId);
		}
		 Map<Double,Integer> treeMap = new TreeMap<Double,Integer>(prioVideo);
		 List<Integer>returnStuff=new LinkedList<Integer>();
		 for(Map.Entry<Double,Integer> entry : treeMap.entrySet()) {
			returnStuff.add(entry.getValue());
		 }
		return returnStuff;
	}

	private int find(Videos v, int id) {
		int size=0;
		List<Video> videos = v.videos;
		for (Video video : videos) {
			if(video.id==id){
				size=video.size
					;
			}
		}
		return size;
	}
	
}
