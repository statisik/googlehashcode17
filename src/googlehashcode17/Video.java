package googlehashcode17;

public class Video {
	public int size;
	public int id;
	
	@Override
	public String toString() {
		return "Video [size=" + size + ", id=" + id + "]";
	}

	public Video(int size, int id) {
		super();
		this.size = size;
		this.id = id;
	}
}
