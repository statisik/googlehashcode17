package googlehashcode17;

import java.util.List;

public class Endpoint {
	public List<CacheData>caches;
	public int datacenterLatency;
	public int id;
	
	public Endpoint(List<CacheData> caches, int latency, int id) {
		this.caches = caches;
		this.datacenterLatency = latency;
		this.id = id;
	}

	public Endpoint() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Endpoint [caches=" + caches + ", datacenterLatency=" + datacenterLatency + ", id=" + id + "]";
	}
}
